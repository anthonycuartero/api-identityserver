﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer
{
    public class DemoDbContext : IdentityDbContext
    {
        public DemoDbContext()
        {

        }

        public DemoDbContext(DbContextOptions<DemoDbContext> options)
            : base(options)
        {

        }

        public DbSet<IdentityUser> IdentityUser { get; set; }
    }
}
