﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ApiProject.Model
{
    public class UserModel
    {
        [EmailAddress]
        [JsonPropertyName("email")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [JsonPropertyName("password")]
        public string Password { get; set; }
    }
}
