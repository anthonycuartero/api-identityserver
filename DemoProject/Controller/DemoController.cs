﻿using ApiProject.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ApiProject.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class DemoController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private static readonly HttpClient _httpClient = new HttpClient();

        public DemoController(
            UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        [HttpPost("signup")]
        public async Task<IActionResult> Signup([FromBody] UserModel userDetails)
        {
            var identityUser = new IdentityUser()
            {
                UserName = userDetails.Email,
                Email = userDetails.Email,
            };
            var result = await _userManager.CreateAsync(identityUser, userDetails.Password);
            if (result.Succeeded)
            {
                return Ok("Successfully registered.");
            }
            return BadRequest();
        }

        [HttpPost("signin")]
        public async Task<IActionResult> Signin([FromBody] UserModel userDetails)
        {
            var values = new Dictionary<string, string>
            {
                { "grant_type", "password" },
                { "client_id", "870255e5f27240a1aeb3ccb27ea58b79" },
                { "client_secret", "a28dd6626fb542fc91bd7aa472f42dcc" },
                { "scope", "apiscope offline_access" },
                { "username", userDetails.Email },
                { "password", userDetails.Password },
            };

            var content = new FormUrlEncodedContent(values);

            var response = await _httpClient.PostAsync("https://localhost:44375/connect/token", content);

            var responseString = await response.Content.ReadAsStringAsync();

            if (((int)response.StatusCode) == 200)
            {
                return Ok(responseString);
            }

            return BadRequest();
        }

        [HttpPost("externallogin")]
        public async Task<IActionResult> ExternalLogin(ExternalLoginModel loginModel)
        {
            var user = new IdentityUser();
            user = await _userManager.FindByEmailAsync(loginModel.Email);

            if (user == null)
            {
                // If user doesn't exist yet then Register
                user = new IdentityUser()
                {
                    Id = loginModel.Id,
                    UserName = loginModel.Email,
                    Email = loginModel.Email,
                };

                await _userManager.CreateAsync(user, "DefaultExternalP@ssword123");
            }

            //Set up identity server config params for login
            var values = new Dictionary<string, string>
            {
                { "grant_type", "password" },
                { "client_id", "870255e5f27240a1aeb3ccb27ea58b79" },
                { "client_secret", "a28dd6626fb542fc91bd7aa472f42dcc" },
                { "scope", "apiscope offline_access" },
                { "username", user.Email },
                { "password", "DefaultExternalP@ssword123" },
            };

            var content = new FormUrlEncodedContent(values);

            // connect to identity server token endpoint
            var response = await _httpClient.PostAsync("https://localhost:44375/connect/token", content); 

            var responseString = await response.Content.ReadAsStringAsync();

            if (((int)response.StatusCode) == 200)
            {
                return Ok(responseString);
            }

            return BadRequest();

        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok("AUTHORIZED!");
        }
    }
}
